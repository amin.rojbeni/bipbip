package projetpi.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import projetpi.dao.daoInterfaces.IMessagerDao;
import projetpi.entity.IdMessager;
import projetpi.entity.IdUser;
import projetpi.entity.Messager;
import projetpi.util.MyDB;

public class MessagerDao implements IMessagerDao{
	private  Connection connect = (Connection) MyDB.getInstance().getCnx();
	@Override
	public void add(Messager t) {
		String query="insert into message (id_user_em,id_user_resp,description,date_creation) values (?,?,?,?);";
		PreparedStatement pstat;
		try {
			pstat=(PreparedStatement) connect.prepareStatement(query);
			pstat.setInt(1,t.getId_user_em());
			pstat.setInt(2,t.getId_user_re());
			pstat.setString(3,t.getDescription());
			pstat.setTimestamp(4, t.getDate_creation());
			pstat.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Messager t) {
		String query="update  message set description=? date_creation=? where id_message=?";
		PreparedStatement pstat;
		try {
			pstat=(PreparedStatement) connect.prepareStatement(query);
			pstat.setString(1,t.getDescription());
			pstat.setTimestamp(2,t.getDate_creation());
			pstat.setInt(3,t.getId_message());
			pstat.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void delete(Messager t) {
		String query="delet from  message weher id_message=?";
		PreparedStatement pstat;
		try {
			pstat=(PreparedStatement) connect.prepareStatement(query);
			pstat.setInt(1,t.getId_message());
			pstat.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Messager findById(IdMessager i) {
		Messager message=new Messager();
		String query="select * from  message weher id_message=?";
		PreparedStatement pstat;
		try {
			pstat=(PreparedStatement) connect.prepareStatement(query);
			pstat.setInt(1,i.getId());
			ResultSet rs=pstat.executeQuery();
			while(rs.next()) {
               message.setId_message(rs.getInt(1));
			   message.setId_user_em(rs.getInt(2));
			   message.setId_user_re(rs.getInt(3));
			   message.setDescription(rs.getString(4));
			   message.setDate_creation(rs.getTimestamp(5));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return message;
	}

	@Override
	public List<Messager> getAll(Messager t) {
		List<Messager> list =new ArrayList<>();
		String query="select * from  message";
		PreparedStatement pstat;
		try {
			pstat=(PreparedStatement) connect.prepareStatement(query);
			ResultSet rs=pstat.executeQuery();
			while(rs.next()) {
				list.add(new Messager(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getString(4),rs.getTimestamp(5)));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Messager> findByIdEmEr(IdUser idEm, IdUser idRe) {
		List<Messager> list =new ArrayList<>();
		String query="select * from  message where id_user_em=? and id_user_resp=?";
		PreparedStatement pstat;
		try {
			pstat=(PreparedStatement) connect.prepareStatement(query);
			pstat.setInt(1,idEm.getId());
			pstat.setInt(2,idRe.getId());
			ResultSet rs=pstat.executeQuery();
			while(rs.next()) {
				list.add(new Messager(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getString(4),rs.getTimestamp(5)));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

}
