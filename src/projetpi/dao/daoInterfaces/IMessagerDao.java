package projetpi.dao.daoInterfaces;

import java.util.List;

import projetpi.entity.IdMessager;
import projetpi.entity.Messager;
import projetpi.entity.IdUser;

public interface IMessagerDao extends IGeneralDao<Messager, IdMessager> {
   public List<Messager>findByIdEmEr(IdUser idEm,IdUser idRe);
   
}
