/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetpi.dao.daoInterfaces;

import projetpi.entity.User;

/**
 *
 * @author amin
 */
public interface IUserDao extends IGeneralDao<User, Integer>{
    
}
