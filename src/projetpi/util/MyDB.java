/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetpi.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 *
 * @author amin
 */
public class MyDB {

   
    private Connection cnx;
    private static MyDB instance=null;
    private MyDB() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            cnx = DriverManager.getConnection(Credentials.url
                    , Credentials.user, Credentials.password);
        } catch (SQLException ex) {
            ex.printStackTrace();
            
        }
        System.out.println("Connexion établie");
    }
    
    public static MyDB getInstance(){
    if(instance == null){
        synchronized (MyDB.class) {
            if(instance == null){
                instance = new MyDB();
            }
        }
    }
    return instance;
}

    public Connection getCnx() {
        return cnx;
    }
    
    

}
