/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetpi.entity;

import projetpi.enums.TypeSexe;

/**
 *
 * @author amin
 */
public class User {
    private int idUser;
    private String nom;
    private String prenom;
    private String role;
    private long numTel;
    private TypeSexe sexe;
    private String email;
    private String password;
    private String login;
    private boolean isBlocked;
}
