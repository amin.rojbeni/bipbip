package projetpi.entity;

import java.sql.Timestamp;

public class Messager {
  private int id_message;
  private int id_user_em;
  private int id_user_re;
  private String description;
  private Timestamp date_creation;
public int getId_message() {
	return id_message;
}
public void setId_message(int id_message) {
	this.id_message = id_message;
}
public int getId_user_em() {
	return id_user_em;
}
public void setId_user_em(int id_user_em) {
	this.id_user_em = id_user_em;
}
public int getId_user_re() {
	return id_user_re;
}
public void setId_user_re(int id_user_re) {
	this.id_user_re = id_user_re;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public Timestamp getDate_creation() {
	return date_creation;
}
public void setDate_creation(Timestamp date_creation) {
	this.date_creation = date_creation;
}
@Override
public String toString() {
	return "Messager [id_message=" + id_message + ", id_user_em=" + id_user_em + ", id_user_re=" + id_user_re
			+ ", description=" + description + ", date_creation=" + date_creation + "]";
}
public Messager(int id_user_em, int id_user_re, String description, Timestamp date_creation) {
	this.id_user_em = id_user_em;
	this.id_user_re = id_user_re;
	this.description = description;
	this.date_creation = date_creation;
}
public Messager(int id,int id_user_em, int id_user_re, String description, Timestamp date_creation) {
	this(id_user_em,id_user_re,description,date_creation);
	this.id_message=id;
}
public Messager() {
	
}
}
